#!/bin/sh
http_response=$(curl -s -o /dev/null -w "%{http_code}" http://$SERVER_IP:$APP_PORT)
if [ $http_response != "200" ]; then
    echo "Server error:"
	exit 1
else
	echo "Server OK:"
	echo $http_response
	exit 0
fi
