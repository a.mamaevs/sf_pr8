data "template_file" "server" {
  template = file("${path.module}/tf_templates/inventory.tpl")
  vars = {
    server = join("\n", formatlist("%s ansible_ssh_host=%s ansible_ssh_user=${var.ssh_user}", yandex_compute_instance.server-sf-pr8.*.name, yandex_compute_instance.server-sf-pr8.*.network_interface.0.nat_ip_address))

  }

}

resource "local_file" "server_file" {
  content  = data.template_file.server.rendered
  filename = "${path.module}/ansible/inventory"
}
