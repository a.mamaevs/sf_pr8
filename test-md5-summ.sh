#!/bin/sh
online_md5="$(curl -sL http://$SERVER_IP:$APP_PORT | md5sum | cut -d ' ' -f 1)"
local_md5="$(md5sum $CI_PROJECT_DIR/index.html | cut -d ' ' -f 1)"
if [ "$online_md5" != "$local_md5" ]; then
    echo "md5 summ not equal!"
	echo $online_md5
	echo $local_md5
    exit 1
else
    echo "md5 summ equal!"
	echo $online_md5
	echo $local_md5
    exit 0	
fi

